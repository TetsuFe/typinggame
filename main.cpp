//Using SDL and standard IO

#include <SDL2/SDL.h>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <stdlib.h>
    
    //Key press surfaces constants
    enum KeyPressSurfaces
    {
        KEY_PRESS_SURFACE_DEFAULT,
        KEY_PRESS_SURFACE_UP,
        KEY_PRESS_SURFACE_DOWN,
        KEY_PRESS_SURFACE_LEFT,
        KEY_PRESS_SURFACE_RIGHT,
        KEY_PRESS_SURFACE_TOTAL
    };

    //Screen dimension constants
    const int SCREEN_WIDTH = 640;
    const int SCREEN_HEIGHT = 480;
    
    //Starts up SDL and creates window
    bool init();
    
    //Loads media
    bool loadMedia();
    
    //Frees media and shuts down SDL
    void close();
    
    //Loads individual image
    SDL_Surface* loadSurface( std::string path );
    
    //The window we'll be rendering to
    SDL_Window* gWindow = NULL;
    
    //The surface contained by the window
    SDL_Surface* gScreenSurface = NULL;
    
    //The images that correspond to a keypress
    SDL_Surface* gKeyPressSurfaces[ KEY_PRESS_SURFACE_TOTAL ];
    
    //Current displayed image
    SDL_Surface* gCurrentSurface = NULL;
    
    //Main loop flag
    bool quit = false;
    
    //Event handler
    SDL_Event e;

    //キー入力された文字をSDLK＿　に変換するために使う
    SDL_Event sdlk;

    Uint32 chTrans(char c, int i);//返り値としてSDLK_ を返す

    int start = 0;

    int i = 0;
    char *str;
    char buf[512];
    
    FILE *fp;
    char fname[128];
    
    int main( int argc, char* args[] )
    {
        printf("使いたいファイルの場所を入力してください: ");
        scanf("%s",fname);
        if((fp = fopen(fname,"r")) == NULL){
            printf("このファイルは開けません\n");
        }
        
        //配列の方に文字を一文字づつ格納
        do {
            printf("%d",i);
            fscanf(fp,"%c",(buf+i));
            i++;
        }while(buf[i] != EOF);
       
        // 注意！sizeは２６４以上でないとmallocエラー
        str = (char *)malloc(sizeof(char)*264);
        for(int k=0; buf[k]!=EOF; k++){
            str[k] = buf[k];
            //左側はポインタ変数、右側は配列に格納された変数(文字)
        }
        
        //ファイルから読み込んだ文字を表示
        printf("%s\n",str);
        i = 0;

        //いつも使うSDLセット Init, loadMedia
        if( !init() )
        {
            printf( "Failed to initialize!\n" );
        }
        else
        {
            //Load media
            if( !loadMedia() )
            {
                printf( "Failed to load media!\n" );
            }
            else
            {
                //Set default current surface
                //gCurrentSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_DEFAULT ];
                
                //閉じるボタンが押されるまで続く
                while( !quit )
                {
                    //なんらかのアクションが起こるまでループが続く
                    while( SDL_PollEvent( &e ) != 0 )
                    {
                        if(start== 0){
                            printf("キーを押してください\n");
                            start = 1;
                        }
                        //User requests quit
                        if( e.type == SDL_QUIT )
                        {
                            quit = true;
                        }
                        //KEY_DOWN,キーが押されたら開始
                        else if( e.type == SDL_KEYDOWN )
                        {
                            printf("str[%d] = %c\n",i,
                                   str[i]);
                            //”正解の”キーに対応するSDLK_を返す関数
                            sdlk.type = chTrans(str[i],i);
                            printf("KEY = %d\n",sdlk.type);
                            
                            //”押された”キー　と　”正解”のキーを比較して正誤判定し、
                            if(e.key.keysym.sym == sdlk.type){
                            printf("i = %d\n",i);
                            printf("sym = %c \n",e.key.keysym.sym);
                            printf("str[%d] = %c \n",i,str[i]);
                            //正解なら次の文字へ、ポインタのアドレスを進める。配列っぽく書いてあるがこれもポインタである
                            i++;
                                //正常に次の文字に移ったかの確認
                            printf("str[%d] = %c \n",i,str[i]);
                        }
                        //次に押すべきキーをナビ
                        printf("\n%cを押してください\n",str[i]);
                    }
                }
            }
        }
        printf("ループ終了\n");
    }
    fclose(fp);
    free(str);
    close();
    return 0;
}
    //main以外の関数
    
    bool init()
    {
        //Initialization flag
        bool success = true;
        
        //Initialize SDL
        if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
        {
            printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
            success = false;
        }
        else
        {
            //Create window
            gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
            if( gWindow == NULL )
            {
                printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
                success = false;
            }
            else
            {
                
                //Get window surfa
                gScreenSurface = SDL_GetWindowSurface( gWindow );//      <- 1回目のSurface系変更
                
            }
        }
        
        return success;
    }
    
    
    void close()
    {
        //Deallocate surface
        SDL_FreeSurface( gCurrentSurface );     //
        gCurrentSurface = NULL;
        
        //Destroy window
        SDL_DestroyWindow( gWindow );
        gWindow = NULL;
        
        //Quit SDL subsystems
        SDL_Quit();
    }
    
    SDL_Surface* loadSurface( std::string path )
    {
        //Load image at specified path
        SDL_Surface* loadedSurface = SDL_LoadBMP( path.c_str() );
        if( loadedSurface == NULL )
        {
            printf( "Unable to load image %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
        }
        
        return loadedSurface;
    }
    
    bool loadMedia()
    {
        //Loading success flag
        bool success = true;
        
        //Load default surface
        gKeyPressSurfaces[ KEY_PRESS_SURFACE_DEFAULT ] = loadSurface( "/Users/femental/Documents/typingGame/testSDL2/04_key_presses/press.bmp" );
        if( gKeyPressSurfaces[ KEY_PRESS_SURFACE_DEFAULT ] == NULL )
        {
            printf( "Failed to load default image!\n" );
            success = false;
        }
        
        //Load up surface
        gKeyPressSurfaces[ KEY_PRESS_SURFACE_UP ] = loadSurface( "/Users/femental/Documents/typingGame/testSDL2/04_key_presses/up.bmp" );
        if( gKeyPressSurfaces[ KEY_PRESS_SURFACE_UP ] == NULL )
        {
            printf( "Failed to load up image!\n" );
            success = false;
        }
        
        //Load down surface
        gKeyPressSurfaces[ KEY_PRESS_SURFACE_DOWN ] = loadSurface( "/Users/femental/Documents/typingGame/testSDL2/04_key_presses/down.bmp" );
        if( gKeyPressSurfaces[ KEY_PRESS_SURFACE_DOWN ] == NULL )
        {
            printf( "Failed to load down image!\n" );
            success = false;
        }
        
        //Load left surface
        gKeyPressSurfaces[ KEY_PRESS_SURFACE_LEFT ] = loadSurface( "/Users/femental/Documents/typingGame/testSDL2/04_key_presses/left.bmp" );
        if( gKeyPressSurfaces[ KEY_PRESS_SURFACE_LEFT ] == NULL )
        {
            printf( "Failed to load left image!\n" );
            success = false;
        }
        
        //Load right surface
        gKeyPressSurfaces[ KEY_PRESS_SURFACE_RIGHT ] = loadSurface( "/Users/femental/Documents/typingGame/testSDL2/04_key_presses/right.bmp" );
        if( gKeyPressSurfaces[ KEY_PRESS_SURFACE_RIGHT ] == NULL )
        {
            printf( "Failed to load right image!\n" );
            success = false;
        }
        
        return success;
    }
Uint32 chTrans( char c, int i){
    SDL_Event e;
    switch(c){
        case 'a' :
        case 'A' :
            printf("sdlkSetOK");
            e.type =  SDLK_a;
            break;
        case 'b' :
        case 'B':
            printf("sdlkSetOK");
            e.type = SDLK_b;
            break;
        case 'c' :
        case 'C' :
            printf("sdlkSetOK");
            e.type = SDLK_c;
            break;
        case 'd' :
        case 'D':
            printf("sdlkSetOK");
            e.type = SDLK_d;
            break;
        case 'e' :
        case 'E' :
            printf("sdlkSetOK");
            e.type = SDLK_e;
            break;
        case 'f' :
        case 'F':
            printf("sdlkSetOK");
            e.type = SDLK_f;
            break;
        case 'g' :
        case 'G' :
            printf("sdlkSetOK");
            e.type = SDLK_g;
            break;
        case 'h' :
        case 'H':
            printf("sdlkSetOK");
            e.type = SDLK_h;
            break;
        case 'i' :
        case 'I' :
            printf("sdlkSetOK");
            e.type = SDLK_i;
            break;
        case 'j' :
        case 'J':
            printf("sdlkSetOK");
            e.type = SDLK_j;
            break;
        case 'k' :
        case 'K' :
            printf("sdlkSetOK");
            e.type = SDLK_k;
            break;
        case 'l' :
        case 'L':
            printf("sdlkSetOK");
            e.type = SDLK_l;
            break;
        case 'm' :
        case 'M' :
            printf("sdlkSetOK");
            e.type = SDLK_m;
            break;
        case 'n' :
        case 'N':
            printf("sdlkSetOK");
            e.type = SDLK_n;
            break;
        case 'o' :
        case 'O' :
            printf("sdlkSetOK");
            e.type = SDLK_o;
            break;
        case 'p' :
        case 'P':
            printf("sdlkSetOK");
            e.type = SDLK_p;
            break;
        case 'q' :
        case 'Q' :
            printf("sdlkSetOK");
            e.type = SDLK_q;
            break;
        case 'r' :
        case 'R':
            printf("sdlkSetOK");
            e.type = SDLK_r;
            break;
        case 's' :
        case 'S' :
            printf("sdlkSetOK");
            e.type = SDLK_s;
            break;
        //case 't':
        case 'T':
            printf("sdlkSetOK");
            e.type = SDLK_t;
            break;
        case 'u' :
        case 'U' :
            printf("sdlkSetOK");
            e.type = SDLK_u;
            break;
        case 'v' :
        case 'V':
            printf("sdlkSetOK");
            e.type = SDLK_v;
            break;
        case 'w' :
        case 'W' :
            printf("sdlkSetOK");
            e.type = SDLK_w;
            break;
        case 'x' :
        case 'X':
            printf("sdlkSetOK");
            e.type = SDLK_x;
            break;
        case 'y' :
        case 'Y' :
            printf("sdlkSetOK");
            e.type = SDLK_y;
            break;
        case 'z':
        case 'Z':
            printf("sdlkSetOK");
            e.type = SDLK_z;
            break;
        case ' ':
            e.type = SDLK_SPACE;
            break;
            //その他の文字も追加しよう
            }
    return e.type;
}
